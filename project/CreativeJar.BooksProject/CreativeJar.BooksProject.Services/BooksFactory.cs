﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace CreativeJar.BooksProject.Services
{
	public class BooksFactory : IBooksFactory
	{
		#region Implementation of IBooksFactory

		public string[] GetListOfFilesInDirectory(string directory)
		{
			string[] files = Directory.GetFiles(directory);
			return files;
		}

		public bool HasBooks(string filePath)
		{
			return true;
		}

		public Catalog ImportBooksFromFile(string filePath)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(Catalog));
			Catalog catalog = (Catalog)serializer.Deserialize(new XmlTextReader(filePath));
			return catalog;
		}

		public bool SaveBooksToDatabase(CatalogBook[] books)
		{
			return true;
		}

		public CatalogBook[] GetBooksFromDatabase()
		{
			return new CatalogBook[2];
		}

		public CatalogBook[] GetFilteredBooks(CatalogBook bookFilter)
		{
			return new CatalogBook[1];
		}

		public bool UpdateBook(CatalogBook book)
		{
			return true;
		}

		public CatalogBook GetBookDetails(string id)
		{
			return new CatalogBook();
		}

		#endregion
	}
}
