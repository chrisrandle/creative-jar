﻿namespace CreativeJar.BooksProject.Services
{
	public interface IBooksFactory
	{
		// 1
		/// <summary> Gets a collection of files from the disk </summary>
		/// <param name="directory">Folder to load files from</param>
		/// <returns>A list of files in the folder</returns>
		string[] GetListOfFilesInDirectory(string directory);

		// 2
		/// <summary> Checks if there are books in the file supplied </summary>
		/// <param name="filePath">The URI for the file</param>
		/// <returns>True if the file contains books</returns>
		bool HasBooks(string filePath);
		
		// 3
		/// <summary> Imports books from a specified file </summary>
		/// <param name="filePath">The URI for the file</param>
		/// <returns>A collection of CatalogBooks loaded from file</returns>
		Catalog ImportBooksFromFile(string filePath);
		
		// 4 
		/// <summary>
		/// Saves books to database. 
		/// </summary>
		/// <param name="books">Array of books to save to disk</param>
		/// <returns>True if successful else false</returns>
		bool SaveBooksToDatabase(CatalogBook[] books);
		
		// 5
		/// <summary>
		/// Retrieves a list of books from a database
		/// </summary>
		/// <returns>An array of books from the database</returns>
		CatalogBook[] GetBooksFromDatabase();
		
		// 6
		/// <summary>
		/// Given an example filter book template, it will perform a search with that book
		/// </summary>
		/// <param name="bookFilter">The example book to search</param>
		/// <returns>A collection of matching books</returns>
		CatalogBook[] GetFilteredBooks(CatalogBook bookFilter);
		
		// 7
		/// <summary>
		/// Updates a book in the database
		/// </summary>
		/// <param name="book">The book details</param>
		/// <returns>True if successfully updated, else false</returns>
		bool UpdateBook(CatalogBook book);

		// 8
		/// <summary>
		/// Retrieves book details by book ID. Throws an exception if none found.
		/// </summary>
		/// <param name="id">The book ID</param>
		/// <returns>A matching book</returns>
		CatalogBook GetBookDetails(string id);
	}
}
