﻿using System;
using CreativeJar.BooksProject.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CreativeJar.BooksProject.UnitTestProject
{
	[TestClass]
	public class BooksFactoryTests
	{
		readonly IBooksFactory _instance = new BooksFactory();

		// 1.1	Should List Files In Directory
		[TestMethod]
		public void ShouldListFilesInDirectory1()
		{
			var expected = new string[0];
			var result = _instance.GetListOfFilesInDirectory("/");

			Type type = expected.GetType();

			Assert.IsTrue(result is string[]);
		}
		// 1.2	Should List Files In Directory
		[TestMethod]
		public void ShouldListFilesInDirectory2()
		{
			int expected = 1;
			var result = _instance.GetListOfFilesInDirectory("/");

			Assert.IsTrue(result.Length >= expected);
		}

		// 2.	Should Confirm File Contains Books
		[TestMethod]
		public void ShouldConfirmFileContainsBooks()
		{
			_instance.HasBooks("/Books.xml");
		}

		// 3.	Should Import Books From File
		[TestMethod]
		public void ShouldImportBooksFromFile()
		{
			_instance.ImportBooksFromFile("/Books.xml");
		}
		
		//4.	Should Save Books to Database
		[TestMethod]
		public void ShouldSaveBooksToDatabase()
		{
			_instance.SaveBooksToDatabase(new CatalogBook[2]);
		}

		// 5. Should Load Books from Database
		[TestMethod]
		public void ShouldLoadBooksFromDatabase()
		{
			_instance.GetBooksFromDatabase();
		}

		// 6.	Should Load and Filter Books from Database
		[TestMethod]
		public void ShouldLoadAndFilterBooksFromDatabase()
		{
			var filter = new CatalogBook {Author = "Ralls"};
			CatalogBook[] result = _instance.GetFilteredBooks(filter);

			Assert.IsTrue(result.Length > 0);
		}

		// 7.	Should Update Books in Database
		[TestMethod]
		public void ShouldUpdateBooksInDatabase()
		{
			var updatedBook = new CatalogBook();
			bool result = _instance.UpdateBook(updatedBook);

			Assert.IsTrue(result);
		}

		// 8.	Should Get Book Details
		[TestMethod]
		public void ShouldGetBookDetails()
		{
			string id = "bk102";
			CatalogBook result = _instance.GetBookDetails(id);

			Assert.IsNotNull(result);
		}
	}
}